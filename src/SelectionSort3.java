/**
 * @author bin.choi
 * @since 2015-11-12.
 */
public class SelectionSort3 {
    void sort(int[] arr) {
        int length = arr.length;
        int minIndex = 0;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }

            swap(arr, i, minIndex);
        }
    }

    private void swap(int[] arr, int i, int minIndex) {
        int temp = arr[i];
        arr[i] = arr[minIndex];

        arr[minIndex] = temp;
    }

    public static void main(String[] args) {

        SelectionSort3 selectionSort3 = new SelectionSort3();
        int[] arr = {5, 2, 6, 8, 32, 2, 6, 8, 0};
        selectionSort3.sort(arr);

        for (int i : arr) {
            System.out.println(i);
        }

    }
}
