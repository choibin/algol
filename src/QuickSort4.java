/**
 * @author bin.choi
 * @since 2016-07-12.
 */
public class QuickSort4 {


    static void partition(int[] arr) {
        if (arr == null || arr.length <= 0) {
            throw new IllegalArgumentException();
        }

        int pivotIndex = arr.length - 1;

        int leftIndex = 0;
        int rightIndex = pivotIndex - 1;

        int finalPivotIndex;

        for (int i = 0; i < pivotIndex - 1; i++) {

            if (leftIndex == rightIndex) {
                finalPivotIndex = rightIndex;
                swap(arr, pivotIndex, rightIndex);
            } else {
                if (arr[leftIndex] > arr[pivotIndex] && arr[rightIndex] < arr[pivotIndex]) {
                    swap(arr, leftIndex, rightIndex);
                }

                if (arr[leftIndex] > arr[pivotIndex] || arr[leftIndex] == arr[pivotIndex]) {
                    leftIndex++;
                }

                if (arr[rightIndex] < arr[pivotIndex] || arr[rightIndex] == arr[pivotIndex]) {
                    rightIndex--;
                }
            }
        }


    }

    static void swap(int[] arr, int leftIndex, int rightIndex) {
        int temp = arr[leftIndex];
        arr[leftIndex] = arr[rightIndex];

        arr[rightIndex] = temp;
    }
}
