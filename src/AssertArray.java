/**
 * @author bin.choi
 * @since 2016-03-18.
 */
public class AssertArray {
    static void isEmpty(int[] arr) {
        if (arr == null || arr.length <= 0) {
            throw new IllegalArgumentException();
        }
    }
}
