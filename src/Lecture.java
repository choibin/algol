import java.util.ArrayList;
import java.util.List;

/**
 * @author bin.choi
 * @since 2016-07-19.
 */
public class Lecture {
    static void sortLetter(String str) {
        if (str.length() % 2 != 0) {
            throw new IllegalArgumentException();
        }

        List<String> twoLetter = new ArrayList<>();
        for (int i = 0; i < str.length(); i+=2) {
            twoLetter.add(str.substring(i, i+2));
        }


        for (int i = 1; i < twoLetter.size(); i++) {
            int tempHashCode = twoLetter.get(i).hashCode();
            String tempString = twoLetter.get(i);
            int j = i - 1;
            while (j >= 0 && twoLetter.get(j).hashCode() > tempHashCode) {
                twoLetter.set(j + 1, twoLetter.get(j));
                j--;
            }

            twoLetter.set(j + 1, tempString);

        }

        twoLetter.forEach(System.out::println);

    }

    public static void main(String[] args) {
        sortLetter("abbaaccb");
    }
}
