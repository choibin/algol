/**
 * @author bin.choi
 * @since 2015-10-27.
 */
public class InsertionSort {
    public int[] sort(int[] arr) {
        for (int index = 1; index < arr.length; index++) {
            int aux = index - 1;
            int temp = arr[index];

            while (aux >= 0 && temp < arr[aux]) {
                arr[aux + 1] = arr[aux];
                aux--;
            }

            arr[aux + 1] = temp;
        }

        return arr;
    }

    public static void main(String[] args) {
        int[] sort = new InsertionSort().sort(new int[]{5, 9, 23, 1, 0, 4, 129});
        for (int i : sort) {
            System.out.println(i);
        }
    }
}
