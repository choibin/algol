import java.util.Arrays;

/**
 * @author bin.choi
 * @since 2015-11-03.
 */
public class MyStack {

    private Object[] elements;
    private int size = 0;

    private static int DEFAULT_CAPACITY = 10;

    public MyStack() {
        this.elements = new Object[DEFAULT_CAPACITY];
    }

    public void push(Object o) {
        ensureCapacity();
        elements[size++] = o;
    }

    private void ensureCapacity() {
        if (size == elements.length) {
            elements = Arrays.copyOf(elements, 2 * size + 1);
        }
    }


}
