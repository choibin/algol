/**
 * @author bin.choi
 * @since 2015-11-12.
 */
public class InsertionSort3 {
    static void sort(int[] arr) {
        int length = arr.length;
        for (int i = 1; i < length; i++) {
            int n = i;

            while (n > 0 && arr[i] > arr[n - 1]) {
                n--;
            }

            swap(arr, i, n);
        }
    }

    private static void swap(int[] arr, int i, int minIndex) {
        int temp = arr[i];
        arr[i] = arr[minIndex];

        arr[minIndex] = temp;
    }

    public static void main(String[] args) {
        int[] arr = {31, 25, 12, 22, 11};
        sort(arr);
        for (int i : arr) {
            System.out.println(i);
        }

    }
}
