/**
 * @author bin.choi
 * @since 2015-10-29.
 */
public class BinarySearch {
    int search(int[] arr, int x) {
        int lo = 0;
        int hi = arr.length - 1;

        while (lo <= hi) {
            int mid = (lo + hi) / 2;

            if (arr[mid] > x) {
                hi = mid - 1;
            }

            if (arr[mid] < x) {
                lo = mid + 1;
            }

            if (arr[mid] == x) {
                return mid;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        System.out.println(new BinarySearch().search(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 3));
    }

}
