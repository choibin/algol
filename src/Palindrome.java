/**
 * @author bin.choi
 * @since 2015-11-03.
 */
public class Palindrome {
    boolean check(String str) {
        int beginIndex = str.length() / 2;

        for (int i = 0; i < beginIndex; i++) {
            if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(new Palindrome().check("abba"));
    }
}
