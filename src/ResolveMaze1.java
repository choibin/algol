/**
 * Created by binchoi on 2016. 8. 11..
 */
public class ResolveMaze1 {
    private static final int PATH_WAY = 0;
    private static final int WALL = 1;
    private static final int VISITED = 3;
    private static final int DEAD_VISITED = 4;

    private static final int[][] maze = new int[][]{
            {0, 0, 0, 0, 0, 0, 0, 1},
            {0, 1, 1, 0, 1, 1, 0, 1},
            {0, 0, 0, 1, 0, 0, 0, 1},
            {0, 1, 0, 0, 1, 1, 0, 0},
            {0, 1, 1, 1, 0, 0, 1, 1},
            {0, 1, 0, 0, 0, 1, 0, 1},
            {0, 0, 0, 1, 0, 0, 0, 1},
            {0, 1, 1, 1, 0, 1, 0, 0}
    };

    static boolean findPath(int x, int y) {
        int N = maze.length;

        if (x < 0 || x >= N || y < 0 || y >= N) {
            return false;
        }

        if (maze[x][y] != PATH_WAY) {
            return false;
        }

        if (x == N - 1 && y == N - 1) {
            maze[x][y] = VISITED;
            return true;
        } else {
            maze[x][y] = VISITED;
            if (findPath(x, y - 1) || findPath(x + 1, y) || findPath(x, y + 1) || findPath(x - 1, y)) {
                return true;
            }

            maze[x][y] = DEAD_VISITED;
            return false;
        }
    }

    public static void main(String[] args) {
        printMaze();
        System.out.println(findPath(0, 0));
        printMaze();
    }

    private static void printMaze() {
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                System.out.print(maze[i][j] + " ");
            }
            System.out.println();
        }
    }
}