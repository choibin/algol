import java.util.*;

/**
 * @author bin.choi
 * @since 2015-11-05.
 */
public class Algol {
    String reverse(String str) {
        char[] chars = str.toCharArray();
        int strLen = str.length() - 1;
        for (int i = 0; i < str.length() / 2; i++) {
            char temp = chars[i];
            chars[i] = chars[strLen];
            chars[strLen] = temp;
            strLen--;
        }

        return new String(chars);
    }

    Map<Character, Integer> charCount(String str) {
        char[] chars = str.toCharArray();
        Map<Character, Integer> map = new HashMap<>();

        for (char c : chars) {
            if (!map.containsKey(c)) {
                map.put(c, 0);
            }
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            }
        }

        return map;
    }


    static boolean closeBracket(String str) {
        List<String> open = Arrays.asList("{", "[", "(", "<");
        List<String> close = Arrays.asList("}", "]", ")", ">");

        Stack<String> openStack = new Stack<>();

        for (int i = 0; i < str.length(); i++) {

            for (int j = 0; j < open.size(); j++) {
                if (open.get(j).equals(String.valueOf(str.charAt(i)))) {
                    openStack.push(String.valueOf(str.charAt(i)));

                }
            }

            for (int j = 0; j < close.size(); j++) {
                if (close.get(j).equals(String.valueOf(str.charAt(i)))) {
                    if (openStack.empty()) return false;
                    if (openStack.lastElement().equals(open.get(j))) {
                        openStack.pop();
                    }
                }
            }
        }
        return openStack.empty();

    }

    static String zipString(String str) {
        int count = 1;
        StringBuilder sb = new StringBuilder();
        sb.append(str.charAt(0));

        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) == str.charAt(i - 1)) {
                count++;
            } else {
                sb.append(count);
                sb.append(str.charAt(i));
                count = 1;
            }
        }

        sb.append(count);

        return sb.toString();
    }

    static void numberCount(int[] arr, int x) {
        int length = arr.length;
        int cnt = 0;

        for (int i = 0; i < length; i++) {
            if (arr[i] == x) {
                cnt++;
            }
        }

        System.out.println(cnt);
    }

    static void intToKorean(int val) {
        String commaVal = addComma(val);

        String[] numKo = {"", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구"};
        String[] digitKo = {"십", "백", "천", "만", "억"};

        String[] split = commaVal.split(",");
        for (String s : split) {
            for (int i = 0; i < s.length(); i++) {

            }
        }

        //947,829,111

    }

    static void arraySum(int[] arr) {
        int length = arr.length;
        int maxSum = 0;

        for (int i = 0; i < length; i++) {
            int arraySum = 0;

            for (int j = i; j < length; j++) {
                arraySum += arr[j];
                if (arraySum > maxSum) {
                    maxSum = arraySum;
                }
            }
        }

        System.out.println(maxSum);
    }

    private static String addComma(int val) {
        String strVal = String.valueOf(val);
        int length = strVal.length();
        int n = length % 3;

        StringBuilder sb = new StringBuilder();

        if (n > 0) {
            for (int i = 0; i < n; i++) {
                sb.append(strVal.charAt(i));
            }

            if (length > 3) {
                sb.append(",");
            }
        }

        int cnt = 0;
        for (int i = n; i < length; i++) {
            sb.append(strVal.charAt(i));
            if ((cnt + 1) % 3 == 0 && i + 1 < length) {
                sb.append(",");
            }

            cnt++;
        }

        return sb.toString();
    }


    static void movingAverage(int[] arr, int m) {
        if (arr == null || m <= 0) {
            throw new IllegalArgumentException();
        }

        int length = arr.length;
        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += arr[i];

            if ((i + 1) >= m) {
                System.out.println(sum / m);
                sum -= arr[i - (m - 1)];
            }
        }
    }

    public static void main(String[] args) {
        //intToKorean(947829111);
        movingAverage(new int[]{60, 70, 43, 78, 61, 55, 17, 100, 48}, 3);
    }
}
