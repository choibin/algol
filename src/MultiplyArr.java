public class MultiplyArr {
    public int[] multipleArr(int[] arr) {
        int[] mArr = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            int mVal = 1;
            for (int j = 0; j < arr.length; j++) {
                if (j != i) {
                    mVal *= arr[j];
                }
            }
            mArr[i] = mVal;
        }

        return mArr;
    }

    public static void main(String[] args) {
        MultiplyArr multiplyArr = new MultiplyArr();
        int[] ints = multiplyArr.multipleArr(new int[]{1, 2, 3, 4, 5});
        for (int anInt : ints) {
            System.out.println(anInt);
        }
    }
}
