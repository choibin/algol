/**
 * @author bin.choi
 * @since 2015-11-09.
 */
public class Fibonacci {
    static void fib(int n) {
        int first = 0;
        int second = 1;

        for (int i = 0; i < n; i++) {

            if (i <= 1) {
                System.out.print(i + " ");
            } else {
                System.out.print((first + second) + " ");
                int temp = first;
                first = second;
                second = temp + second;
            }
        }
    }

    static int fibonacci(int n) {
        if (n < 2) {
            return n;
        }

        int fibonacci1 = fibonacci(n - 1);

        int fibonacci2 = fibonacci(n - 2);

        return fibonacci1 + fibonacci2;

    }

    public static void main(String[] args) {
        System.out.println(fibonacci(10));
    }
}
