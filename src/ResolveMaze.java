/**
 * Created by binchoi on 2016. 5. 27..
 */
public class ResolveMaze {

    static final int X_MIN_EDGE = 0;
    static final int X_MAX_EDGE = 1;
    static final int Y_MIN_EDGE = 2;
    static final int Y_MAX_EDGE = 3;


    static int[][] maze =
            {
                    {0,1,0,0,0},
                    {0,1,1,1,0},
                    {0,1,0,1,0},
                    {0,0,0,1,3},
                    {1,0,0,0,0}
            };

    static boolean isEscape(int x, int y) {
        if(x > maze[0].length || y > maze.length || x < 0 || y < 0) {
            throw new IllegalArgumentException("범위를 벗어났습니다");
        }

        if(maze[y][x] == 0) {
            throw new IllegalArgumentException("길이 아닙니다");
        }

        if(maze[y][x] == 3) return true;


        isEscape(x + 1, y);
        isEscape(x - 1, y);
        isEscape(x, y + 1);
        isEscape(x, y - 1);

        if (y >= maze[0].length - 1 && x < maze.length) {
            if (maze[y][x + 1] == 1 || maze[y][x + 1] == 3) isEscape(x, y + 1);
            if (maze[y - 1][x] == 1 || maze[y][x + 1] == 3) isEscape(x, y + 1);
        }

        if (y < maze[0].length - 1 && x >= maze.length) {
            if (maze[y + 1][x] == 1 || maze[y + 1][x] == 3) isEscape(x + 1, y);
        }

        if (y < maze[0].length && x < maze.length && y > 0 && x > 0) {
            if (maze[y + 1][x] == 1 || maze[y + 1][x] == 3) isEscape(x, y + 1);

            if (maze[y][x + 1] == 1 || maze[y][x + 1] == 3) isEscape(x + 1, y);

            if (maze[y - 1][x] == 1 || maze[y - 1][x] == 3) isEscape(x, y - 1);

            if (maze[y][x - 1] == 1 || maze[y][x - 1] == 3) isEscape(x - 1, y);
        }

        return false;
    }


    public static void main(String[] args) {
        System.out.println(isEscape(1, 0));
    }

}
