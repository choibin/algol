import java.util.HashMap;
import java.util.Map;

/**
 * Created by binchoi on 2016. 7. 21..
 */
public class UriDecoding {
    static void decoding(String url) {
        Map<String, String> map = new HashMap<>();
        map.put("%20", " ");
        map.put("%21", "!");
        map.put("%24", "$");
        map.put("%25", "%");
        map.put("%28", "(");
        map.put("%29", "(");
        map.put("%2a", "*");

        char[] ch = new char[url.length()];

        for (int i = 0; i < url.length(); i++) {
            if (i < url.length() - 2 && "%".equals(url.substring(i, i + 1))) {
                if (map.containsKey(url.substring(i, i + 3))) {
                    ch[i] = map.get(url.substring(i, i + 3)).charAt(0);
                    i+=2;
                }
            } else {
                ch[i] = url.charAt(i);
            }
        }

        for (int i = 0; i < ch.length; i++) {
            System.out.print(ch[i]);
        }
    }

    public static void main(String[] args) {
        decoding("%2520");

    }

}
