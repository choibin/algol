public class ArrSum {
    int arrSum(int[] arr) {
        int currentSum = arr[0];
        int maxSum = arr[0];

        for (int i = 1; i < arr.length; i++) {
            int calc = currentSum + arr[i];
            currentSum = calc >= arr[i] ? calc : arr[i];
            maxSum = maxSum >= currentSum ? maxSum : currentSum;
        }

        return maxSum;
    }

    public static void main(String[] args) {
        ArrSum arrSum = new ArrSum();
        System.out.println(arrSum.arrSum(new int[]{-1, 3, -1, 5}));
    }


}
