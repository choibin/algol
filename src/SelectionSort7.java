/**
 * Created by binchoi on 2016. 4. 3..
 */
public class SelectionSort7 {
    static void sort(int[] arr) {
        if (arr == null || arr.length <= 0) {
            throw new IllegalArgumentException();
        }

        for (int i = 1; i < arr.length; i++) {
            int key = arr[i];

            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = key;
        }
    }

    public static void main(String[] args) {
        int[] arr = {7, 2, 8, 5, 3, 1};
        sort(arr);

        for (int i : arr) {
            System.out.println(i);
        }
    }

}
