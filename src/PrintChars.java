/**
 * Created by binchoi on 2016. 8. 11..
 */
public class PrintChars {
    static void printCharReverse(String str) {
        if (str.equals("")) {
            return;
        }

        printCharReverse(str.substring(1));
        System.out.print(str.charAt(0));
    }

    public static void main(String[] args) {
        printCharReverse("hello world");
    }
}
