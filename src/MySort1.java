/**
 * @author bin.choi
 * @since 2016-03-18.
 */
public class MySort1 {
    static void selectionSort(int[] arr) {
        AssertArray.isEmpty(arr);
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[minIndex] > arr[j]) {
                    minIndex = j;
                }
            }

            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;

        }
    }

    static void insertionSort(int[] arr) {
        AssertArray.isEmpty(arr);

        for (int i = 1; i < arr.length; i++) {
            int index = i;
            while (index > 0 && arr[index] < arr[index - 1]) {
                int temp = arr[index];
                arr[index] = arr[index - 1];
                arr[index - 1] = temp;

                index--;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {6, 2, 8, 3, 1, 9};
        //selectionSort(arr);
        insertionSort(arr);
        for (int i : arr) {
            System.out.println(i);
        }
    }
}
