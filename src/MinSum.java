import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class MinSum {
    public int getMinSum(int[] arr1, int[] arr2) {
        if ((arr1 == null || arr1.length <= 0) || (arr2 == null || arr2.length <= 0)) {
            throw new IllegalArgumentException("배열을 확인하시오.");
        }

        if (arr1.length != arr2.length) {
            throw new IllegalArgumentException("두 배열의 길이는 동일해야합니다.");
        }
        Arrays.sort(arr1);
        Arrays.sort(arr2);
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + ", ");
        }
        System.out.println();
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + ", ");
        }
        System.out.println();

        int totalSum = 0;
        for (int i = 0; i < arr1.length; i++) {
            int minVal = arr1[i];
            int maxVal = arr2[arr2.length - 1 - i];

            int sum = minVal * maxVal;
            if (arr1[i] > arr2[i]) {
                if (arr2[i] * arr1[arr1.length - 1 - i] < sum) {
                    minVal = arr2[i];
                    maxVal = arr1[arr1.length - 1 - i];
                }
            }

            System.out.println("minVal["+i+"]:" + minVal);
            System.out.println("maxVal[]:" + maxVal);

            totalSum += minVal * maxVal;
        }

        return totalSum;
    }

    public static void main(String[] args) {
        MinSum minSum = new MinSum();
        System.out.println(minSum.getMinSum(new int[]{8512, 1448, 7589, 4504, 29, 2219, 6092, 5862, 9443, 5123}, new int[]{7844, 1176, 1722, 7898, 447, 88, 5335, 550, 4621, 2055}));
    }
}
