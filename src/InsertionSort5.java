/**
 * Created by binchoi on 2016. 8. 9..
 */
public class InsertionSort5 {

    static void sort(int[] arr) {

        for (int i = 1; i < arr.length; i++) {
            int x = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j] > x) {
                arr[j + 1] = arr[j];
                j--;
            }

            arr[j + 1] = x;
        }
    }

    public static void main(String[] args) {
        int[] arr = {4, 1, 7, 5, 9, 2};

        sort(arr);

        for (int i : arr) {
            System.out.println(i);
        }
    }

}
