/**
 * @author bin.choi
 * @since 2016-03-18.
 */
public class Palindrome2 {

    static boolean isPalindrome(String word) {
        StringBuilder reverseWord = new StringBuilder();

        for (int i = word.length(); i > 0; i--) {
            reverseWord.append(word.charAt(i - 1));
        }

        return word.equals(reverseWord.toString());
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(""));
    }
}
