import java.util.Random;

/**
 * Created by binchoi on 2017. 1. 24..
 */
public class IntArrayGenerator {
  public static int[] arrGenerate(int size) {
    return new Random()
        .ints(0, size * 5)
        .limit(size)
        .toArray();
  }
}
