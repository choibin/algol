import java.util.ArrayList;
import java.util.List;

/**
 * @author bin.choi
 * @since 2016-03-18.
 */
public class Average {
    static List<Integer> getAverage(int[] arr, int x) {
        if (arr == null || arr.length <= 0) {
            throw new IllegalArgumentException();
        }

        if (arr.length < x) {
            throw new IllegalArgumentException();
        }

        List<Integer> avg = new ArrayList<>(arr.length);

        int firstAvg = 0;
        for (int i = 0; i < x; i++) {
            firstAvg += arr[i];
        }
        avg.add(firstAvg / x);

        for (int i = x; i < arr.length; i++) {
            int j = i / x - 1;
            firstAvg -= arr[j];
            firstAvg += arr[i];

            avg.add(firstAvg / x);
        }

        return avg;
    }

    public static void main(String[] args) {
        getAverage(new int[]{60, 79, 51, 55, 68, 90, 87, 66}, 3).forEach(System.out::println);
    }
}
