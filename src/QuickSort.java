/**
 * @author bin.choi
 * @since 2015-10-27.
 */
public class QuickSort {

    public void sort(int left, int right, int[] arr) {
        if (left == right) {
            return;
        }

        int holdRight = right;
        int holdLeft = left;

        int pivot = arr[right];


        while (left < right) {
            while (arr[left] <= pivot && (left < right)) {
                left++;
            }

            while (arr[right] >= pivot && (left < right)) {
                right--;
            }

            int temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;
        }

        int temp = arr[left];
        arr[left] = pivot;
        arr[holdRight] = temp;


        if (left >= 1) {
            sort(holdLeft, left - 1, arr);
        }

        if (left < arr.length - 1) {
            sort(left + 1, arr.length - 1, arr);
        }

    }

    public static void main(String[] args) {
        int[] arr = {5, 3, 7, 6, 2, 1, 0, 4, 234, 76, 7, 3,98, 0};
        new QuickSort().sort(0, arr.length - 1, arr);

        for (int i : arr) {
            System.out.println(i);
        }
    }

}
