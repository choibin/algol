/**
 * @author bin.choi
 * @since 2015-10-27.
 */
public class SelectionSort {
    public int[] sort(int[] arr) {
        for (int index = 0; index < arr.length; index++) {
            int minIndex = index;
            for (int j = index; j < arr.length; j++) {
                if (arr[minIndex] > arr[j]) {
                    minIndex = j;
                }
            }

            int temp = arr[index];
            arr[index] = arr[minIndex];
            arr[minIndex] = temp;
        }

        return arr;
    }

    public static void main(String[] args) {
        int[] sort = new SelectionSort().sort(new int[]{7, 5, 34, 2, 8, 6, 43, 3, 2, 1, 0});
        for (int i : sort) {
            System.out.println(i);
        }
    }

}
