/**
 * @author bin.choi
 * @since 2016-01-21.
 */
public class SelectionSort5 {
    static void sort(int[] arr) {

        int N = arr.length;
        for (int i = 0; i < N; i++) {
            int minIndex = i;
            for (int j = i; j < N; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }

            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }

        for (int i : arr) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        sort(new int[]{6, 3, 1, 7, 8, 5});
    }
}
