public class FindSubArr {
    public boolean isSubArr(char[] a, char[] b) {
        if(a.length <= 0 || b.length <= 0) return false;
        if(b.length > a.length) return false;

        int[] chkArr = new int[128];

        for (char c : a) {
            chkArr[c]++;
        }

        for (char c : b) {
            chkArr[c]--;
        }

        for (char c : b) {
            if(chkArr[c] != 0) return false;
        }

        return true;
    }

    public static void main(String[] args) {
        char[] a = {'a', 'b', 'c', 'd', 'e', 'f', 'g'};
        char[] b = {'f', 'd', 'e', 'f', 'g'};

        FindSubArr findSubArr = new FindSubArr();
        System.out.println(findSubArr.isSubArr(a, b));
    }
}
