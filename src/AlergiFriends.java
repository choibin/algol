import java.util.*;

/**
 * @author bin.choi
 * @since 2016-05-20.
 */
public class AlergiFriends {

    static void makeDinner(List<String> friends, int ableCookCount, Map<Integer, List<String>> ableEatInfo) {
        if (ableCookCount != ableEatInfo.size()) {
            throw new IllegalArgumentException("할줄아는 음식의수와 먹을수있는 음식의 수가 다릅니다.");
        }


    }

    public static void main(String[] args) {
        List<String> friends = Arrays.asList(new String[]{"bin", "yeon", "yeryeong", "yeojin"});
        int ableCookCount = 6;

        Map<Integer, List<String>> ableEatInfo = new LinkedHashMap<>();
        ableEatInfo.put(2, Arrays.asList("yeryeong", "yeojin"));
        ableEatInfo.put(2, Arrays.asList("bin", "yeojin"));
        ableEatInfo.put(2, Arrays.asList("bin", "yeryeong"));
        ableEatInfo.put(1, Arrays.asList("bin"));
        ableEatInfo.put(2, Arrays.asList("yeon", "yeryeong"));
        ableEatInfo.put(2, Arrays.asList("yeon", "yeojin"));

        makeDinner(friends, ableCookCount, ableEatInfo);
    }
}
