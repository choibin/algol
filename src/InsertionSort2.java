/**
 * @author bin.choi
 * @since 2015-11-09.
 */
public class InsertionSort2 {
    static void sort(int[] arr) {
        int n = arr.length;
        for (int i = 1; i < n; i++) {
            int j = i;

            while (j > 0 && arr[j] < arr[j - 1]) {
                swap(arr, j, j - 1);
                j--;
            }

        }
    }

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        int[] arr = {5, 9, 23, 1, 0, 4, 129};
        sort(arr);

        for (int i : arr) {
            System.out.println(i);
        }
    }
}
