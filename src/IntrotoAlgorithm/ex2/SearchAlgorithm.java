package IntrotoAlgorithm.ex2;

public class SearchAlgorithm {
    public boolean search(int[] arr, int v) {
        boolean flag = false;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == v) flag = true;
        }

        return flag;
    }
}
