package IntrotoAlgorithm.ex2;

public class SelSort {
    public int[] selSort(int[] arr) {
        if(arr == null || arr.length <= 0) throw new IllegalArgumentException();

        int N = arr.length;
        for (int i = 0; i < N; i++) {
            int minIndex = i;
            for (int j = i + 1; j < N; j++) {
                if(arr[j] < arr[minIndex]) minIndex = j;
            }

            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }

        return arr;
    }

    public static void main(String[] args) {
        SelSort selSort = new SelSort();
        int[] ints = selSort.selSort(new int[]{5, 2, 9, 4, 12, 8, 77, 2, 121});
        for (int anInt : ints) {
            System.out.println(anInt);
        }
    }
}
