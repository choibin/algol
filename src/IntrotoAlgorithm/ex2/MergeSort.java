package IntrotoAlgorithm.ex2;

import java.util.Arrays;

public class MergeSort {
    public int[] mergeSort(int[] arr, int p, int r) {

        if (p < r) {
            int q = (p + r) / 2;
            mergeSort(arr, p, q);
            mergeSort(arr, q + 1, r);
            merge(arr, p, q, r);
        }

        return arr;
    }

    public void merge(int[] arr, int p, int q, int r) {
        int n1 = q - p + 1;
        int n2 = r - q;
        int[] lArr = new int[n1];
        int[] rArr = new int[n2];

        for (int i = 0; i < n1; i++) {
            lArr[i] = arr[i + p];
        }

        for (int i = 0; i < n2; i++) {
            rArr[i] = arr[i + q + 1];
        }

//        lArr[n1] = Integer.MAX_VALUE;
//        rArr[n2] = Integer.MAX_VALUE;

        int i = 0;
        int j = 0;

        int x = 0;
        int point = 0;
        for (int k = p; k <= r; k++) {
            if (i == n1) {
                x = 1;
                point = k;
                break;
            } else if (j == n2) {
                x = 2;
                point = k;
                break;
            }
            if (lArr[i] < rArr[j]) {
                arr[k] = lArr[i];
                i++;
            } else if (rArr[j] < lArr[i]) {
                arr[k] = rArr[j];
                j++;
            }
        }

        if (x == 1) {
            for (int k = point; k <= r; k++) {
                arr[k] = rArr[j];
                j++;
            }
        } else if (x == 2) {
            for (int k = point; k <= r; k++) {
                arr[k] = lArr[i];
                i++;
            }
        }
    }

    public static void main(String[] args) {
        MergeSort mergeSort = new MergeSort();
        int[] arr = {3, 41, 52, 26, 38, 57, 9};
        int[] ints = mergeSort.mergeSort(arr, 0, arr.length - 1);

        Arrays.stream(ints).forEach(System.out::println);
    }
}
