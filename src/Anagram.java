/**
 * @author bin.choi
 * @since 2016-03-18.
 */
public class Anagram {
    static boolean isPalindrome(String word1, String word2) {
        if (word1.length() != word2.length()) {
            return false;
        }

        boolean[] flag = new boolean[128];
        for (int i = 0; i < word1.length(); i++) {
            flag[word1.charAt(i)] = true;
        }

        for (int i = 0; i < word2.length(); i++) {
            flag[word2.charAt(i)] = false;
        }

        for (int i = 0; i < flag.length; i++) {
            if (flag[i]) {
                return !flag[i];
            }
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome("madam", "adamm"));
    }
}
