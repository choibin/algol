/**
 * @author bin.choi
 * @since 2016-01-21.
 */
public class SelectionSort1 {
    static void sort(int[] arr) {
        int N = arr.length;
        int minIndex;

        for (int i = 0; i < N; i++) {
            minIndex = i;
            for (int j = i; j < N; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }

            int temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
        }

        for (int i : arr) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        sort(new int[]{6, 2, 1, 5, 3});
    }
}
