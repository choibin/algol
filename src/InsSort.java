public class InsSort {
    public int[] insertionSort(int[] arr) {
        if(arr == null || arr.length <= 0) throw new IllegalArgumentException();

        for (int i = 1; i < arr.length; i++) {
            int key = arr[i];
            int j = i - 1;

            boolean flag = false;
            while (j >= 0 && arr[j] < key) {
                flag = true;
                arr[j + 1] = arr[j];
                j--;
            }

            if(flag) arr[j + 1] = key;
        }

        return arr;
    }

    public static void main(String[] args) {
        InsSort insSort = new InsSort();
        int[] ints = insSort.insertionSort(new int[]{5, 2, 9, 4, 12, 8, 77, 2, 121});
        for (int anInt : ints) {
            System.out.println(anInt);
        }
    }
}
