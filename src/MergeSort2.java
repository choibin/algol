/**
 * @author bin.choi
 * @since 2016-03-11.
 */
public class MergeSort2 {
    static void partition(int[] arr, int begin, int end) {
        if (arr == null || arr.length <= 0) {
            throw new IllegalArgumentException();
        }

        int length = (end + 1) - begin;
        int mid = length == 2 ? begin : (begin + end) / 2;

        if (length >= 2) {
            partition(arr, begin, mid);
            partition(arr, mid + 1, end);

            merge(arr, begin, mid, end);
        }
    }

    private static void merge(int[] arr, int begin, int mid, int end) {
        int leftLength = 1;
        int rightLength = 1;

        if (mid > 0) {
            leftLength = mid - begin + 1;
            rightLength = end - mid;
        }

        int[] leftArr = new int[leftLength + 1];
        int[] rightArr = new int[rightLength + 1];

        for (int i = begin, j = 0; i <= mid; i++, j++) {
            leftArr[j] = arr[i];
        }
        leftArr[leftLength] = Integer.MAX_VALUE;

        for (int i = mid + 1, j = 0; i <= end; i++, j++) {
            rightArr[j] = arr[i];
        }
        rightArr[rightLength] = Integer.MAX_VALUE;

        int l = 0;
        int r = 0;
        for (int i = begin; i <= end; i++) {
            if (leftArr[l] < Integer.MAX_VALUE && leftArr[l] < rightArr[r]) {
                arr[i] = leftArr[l];
                l++;
            } else if (rightArr[r] < Integer.MAX_VALUE && rightArr[r] < leftArr[l]) {
                arr[i] = rightArr[r];
                r++;
            }

        }
    }

    public static void main(String[] args) {
        int[] arr = {5, 2, 7, 19, 11, 6, 10};
        partition(arr, 0, arr.length - 1);

        for (int i : arr) {
            System.out.println(i);
        }
    }
}
