/**
 * Created by binchoi on 2017. 1. 26..
 */
public class Sort1 {

  static void sort1(int[] arr) {
    for (int i = 0; i < arr.length; i++) {
      int minIndex = i;

      for (int j = i; j < arr.length; j++) {
        if (arr[j] < arr[minIndex]) {
          minIndex = j;
        }
      }

      arrSwap(arr, minIndex, i);
    }
  }

  static void sort2(int[] arr) {
    for (int i = 1; i < arr.length; i++) {
      int index = i - 1;
      int temp = arr[i];
      System.out.println("+++++++++++++++++++++ 현재 인덱스: " + i + "++++++++++++++++++++++");
      while (index >= 0 && arr[index] > temp) {
        System.out.println(index + " 번째까지 검사");
        arr[index + 1] = arr[index];
        index--;
      }

      arr[index + 1] = temp;

      for (int i1 : arr) {
        System.out.print(i1 + " ");
      }
      System.out.println();
    }
  }

  private static void arrSwap(int[] arr, int minIndex, int i) {
    int temp = arr[i];
    arr[i] = arr[minIndex];
    arr[minIndex] = temp;
  }

  public static void main(String[] args) {
    int[] arr = IntArrayGenerator.arrGenerate(50);
    long start = System.currentTimeMillis();
    sort2(arr);
    long end = System.currentTimeMillis();
    System.out.println("수행시간: " + (end - start) + " ms");

    for (int i : arr) {
      System.out.print(i + " ");
    }
  }

}
