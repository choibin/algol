/**
 * @author bin.choi
 * @since 2016-01-20.
 */
public class InsertionSort1 {
    static void sort(int[] arr) {
        int N = arr.length;
        for (int i = 1; i < N; i++) {
            int j = i - 1;
            int key = arr[i];

            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j--;
            }
            j += 1;
            arr[j] = key;
        }

        for (int i : arr) {
            System.out.println(i);
        }

    }

    public static void main(String[] args) {
        sort(new int[]{5, 2, 1, 6, 9});
    }
}
