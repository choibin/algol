/**
 * @author bin.choi
 * @since 2016-03-04.
 */
public class MergeSort {
    static void partition(int[] arr, int begin, int end) {
        if (arr == null || arr.length <= 0) {
            throw new IllegalArgumentException();
        }

        int length = end - begin;
        int mid = (begin + end) / 2;

        if (length >= 1) {
            partition(arr, begin, mid);
            partition(arr, mid + 1, end);
            merge(arr, begin, mid, end);
        }

    }

    static void merge(int[] arr, int begin, int mid, int end) {
        int leftArr[];
        int rightArr[];

        if (end - begin == 1) {
            leftArr = new int[2];
            rightArr = new int[2];
        } else {
            leftArr = new int[mid - begin + 2];
            rightArr = new int[end - mid + 1];
        }

        int l = 0;
        int r = 0;

        for (int i = begin; i <= mid; i++, l++) {
            leftArr[l] = arr[i];
        }
        leftArr[leftArr.length - 1] = Integer.MAX_VALUE;

        for (int i = mid + 1; i <= end; i++, r++) {
            rightArr[r] = arr[i];
        }
        rightArr[rightArr.length - 1] = Integer.MAX_VALUE;

        l = 0;
        r = 0;
        for (int i = begin; i <= end; i++) {
            if (leftArr[l] < Integer.MAX_VALUE && leftArr[l] < rightArr[r]) {
                arr[i] = leftArr[l];
                l++;
            } else if (rightArr[r] < Integer.MAX_VALUE && rightArr[r] < leftArr[l]) {
                arr[i] = rightArr[r];
                r++;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {5, 2, 7, 19, 11, 6, 10};
        partition(arr, 0, arr.length - 1);
        for (int i : arr) {
            System.out.println(i);
        }
    }
}
