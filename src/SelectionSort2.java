/**
 * @author bin.choi
 * @since 2015-11-09.
 */
public class SelectionSort2 {
    static void sort(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            int minIndex = i;

            for (int j = i; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }

            swap(arr, i, minIndex);
        }
    }

    private static void swap(int[] arr, int i, int minIndex) {
        int temp = arr[i];
        arr[i] = arr[minIndex];
        arr[minIndex] = temp;
    }

    public static void main(String[] args) {
        int[] arr = {5, 9, 23, 1, 0, 4, 129};
        sort(arr);

        for (int i : arr) {
            System.out.println(i);
        }
    }
}
