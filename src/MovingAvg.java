import java.util.ArrayList;
import java.util.List;

/**
 * Created by binchoi on 2017. 1. 24..
 */
public class MovingAvg {

  static List<Double> movingAverage(double[] weight, int m) {
    List<Double> avgList = new ArrayList<>();

    double weightSum = 0;
    for (int i = 0; i < m - 1; i++) {
      weightSum += weight[i];
    }

    for (int i = m - 1; i < weight.length; i++) {
      weightSum += weight[i];
      avgList.add(weightSum / 3);
      weightSum -= weight[i - m + 1];
    }

    return avgList;
  }

  public static void main(String[] args) {
    double[] weight = new double[]{70, 64, 67, 80, 70, 58, 67, 64, 81, 81, 90, 70};
    List<Double> avgList = movingAverage(weight, 3);

    for (Double avg : avgList) {
      System.out.println(avg);
    }
  }
}
