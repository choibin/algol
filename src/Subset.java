/**
 * @author bin.choi
 * @since 2015-10-29.
 */
public class Subset {
    //배열 a 가 배열 b의 부분집합인지 아닌지 판별
    public boolean subset(int[] a, int[] b) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b.length; j++) {
                if (a[i] == b[j]) {
                    break;
                }

                if (j == b.length - 1 && a[i] != b[j]) {
                    return false;
                }
            }
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(new Subset().subset(new int[]{0, 2, 3}, new int[]{1, 2, 3, 4, 5, 6}));
    }
}
