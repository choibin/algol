/**
 * @author bin.choi
 * @since 2015-10-29.
 */
public class QuickSort2 {
    void qsort(int left, int right, int[] arr) {
        if (left < right) {
            int pi = partition(left, right, arr);

            if (pi > 0) {
                qsort(left, pi - 1, arr);
            }

            if (pi < arr.length - 1) {
                qsort(pi + 1, right, arr);
            }
        }
    }

    int partition(int left, int right, int[] arr) {

        if (left < right) {
            int pivot = arr[right];
            int holdRight = right;

            while (left < right) {
                while (arr[left] <= pivot && left < right) {
                    left++;
                }

                while (arr[right] >= pivot && left < right) {
                    right--;
                }

                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
            }

            int temp = arr[holdRight];
            arr[holdRight] = arr[left];
            arr[left] = temp;
        }

        return left;
    }

    public static void main(String[] args) {
        int[] arr = {7, 5, 34, 8, 9, 1, 4, 12};
        new QuickSort2().qsort(0, arr.length - 1, arr);

        for (int i : arr) {
            System.out.println(i);
        }
    }
}
