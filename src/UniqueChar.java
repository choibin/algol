import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * @author bin.choi
 * @since 2015-11-03.
 */
public class UniqueChar {
    boolean check(String str) {
        int[] countArray = new int[123];

        for (int i = 0; i < str.length(); i++) {
            countArray[str.charAt(i)]++;
        }

        for (int i = 0; i < countArray.length; i++) {
            if (countArray[i] > 1) {
                return false;
            }
        }


        return true;
    }


    public static void main(String[] args) {
        String str = "abcdeefghijklmnopqrstuvwxyz";

        System.out.println(new UniqueChar().check(str));


    }
}
