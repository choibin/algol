/**
 * @author bin.choi
 * @since 2015-11-06.
 */
public class QuickSort3 {
    void qsort(int[] arr, int left, int right) {

        if (left < right) {
            int q = partition(arr, left, right);

            if (q > 0) {
                qsort(arr, left, q - 1);
            }

            if (right < arr.length - 1) {
                qsort(arr, q + 1, right);
            }

        }

    }

    int partition(int[] arr, int left, int right) {

        if (left < right) {

            int pivot = arr[right];
            int holdRight = right;

            while (left < right) {
                while (arr[left] <= pivot && left < right) {
                    left++;
                }

                while (arr[right] >= pivot && left < right) {
                    right--;
                }

                swap(arr, left, right);
            }

            swap(arr, holdRight, left);
        }

        return left;
    }

    private void swap(int[] arr, int left, int right) {
        int temp = arr[left];
        arr[left] = arr[right];
        arr[right] = temp;
    }

    public static void main(String[] args) {
        QuickSort3 q3 = new QuickSort3();

        int[] arr = {7, 5, 34, 8, 9, 1, 4, 12};
        q3.qsort(arr, 0, arr.length - 1);

        for (int i : arr) {
            System.out.println(i);
        }
    }

}
