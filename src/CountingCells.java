/**
 * Created by binchoi on 2016. 8. 12..
 */
public class CountingCells {
    static final int[][] cells = new int[][]{
            {1, 0, 0, 0, 0, 0, 0, 1},
            {0, 1, 1, 0, 0, 1, 0, 0},
            {1, 1, 0, 0, 1, 0, 1, 0},
            {0, 0, 0, 0, 0, 1, 0, 0},
            {0, 1, 0, 1, 0, 1, 0, 0},
            {1, 0, 0, 0, 1, 0, 0, 1},
            {0, 1, 1, 0, 0, 1, 1, 1}
    };

    private static final int BACKGROUND_PIXEL = 0;
    private static final int IMAGE_PIXEL = 1;
    private static final int COUNTED_PIXEL = 2;

    static int countBlob(int x, int y) {
        if (x < 0 || x >= cells[0].length || y < 0 || y >= cells.length) {
            return 0;
        }

        if (cells[y][x] != IMAGE_PIXEL) {
            return 0;
        }

        cells[y][x] = COUNTED_PIXEL;
        return 1 + countBlob(x, y - 1) + countBlob(x + 1, y - 1) + countBlob(x + 1, y) + countBlob(x + 1, y + 1) + countBlob(x, y + 1) + countBlob(x - 1, y + 1) + countBlob(x - 1, y) + countBlob(x - 1, y - 1);
    }

    public static void main(String[] args) {
        System.out.println(countBlob(5, 3));
        printCells();
    }

    private static void printCells() {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                System.out.print(cells[i][j] + " ");
            }
            System.out.println();
        }
    }
}
