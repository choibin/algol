/**
 * Created by binchoi on 2016. 7. 27..
 */
public class Anagram1 {
    private static final String NO = "No.";
    private static final String YES = "Yes";

    static String anagram(String word, String otherWord) {

        if (word.length() != otherWord.length()) {
            return NO;
        }

        if (word.equals(otherWord)) {
            return NO;
        }

        int wordChar = 0;
        for (int i = 0; i < word.length(); i++) {
            wordChar += word.charAt(i);
        }

        int otherWordChar = 0;
        for (int i = 0; i < otherWord.length(); i++) {
            otherWordChar += otherWord.charAt(i);
        }

        return wordChar == otherWordChar ? YES : NO;
    }

    public static void main(String[] args) {
        System.out.println(anagram("weird", "wired"));
    }

}
