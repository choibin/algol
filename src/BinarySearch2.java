/**
 * @author bin.choi
 * @since 2015-11-12.
 */
public class BinarySearch2 {
    static void search(int[] arr, int x) {
        int left = 0;
        int right = arr.length;

        while (left < right) {
            int mid = (left + right) / 2;

            if (x > arr[mid]) {
                left = mid;
            }

            if (x < arr[mid]) {
                right = mid;
            }

            if (x == arr[mid]) {
                left = mid;
                right = mid;
            }
        }

        System.out.println(arr[right]);


    }

    public static void main(String[] args) {
        search(new int[]{1, 2, 3, 4, 5, 6, 7}, 5);
    }
}
