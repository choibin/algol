/**
 * @author bin.choi
 * @since 2015-11-09.
 */
public class PrintDiamond {
    static void print(int x, int y) {
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {

                int leftPoint;
                int rightPoint;
                if (i <= x / 2) {
                    leftPoint = x / 2 - i;
                    rightPoint = x / 2 + i;
                } else {
                    leftPoint = i - (x / 2);
                    rightPoint = x - (i + 1 - x / 2);
                }

                if (j == leftPoint) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }

                if (j == rightPoint) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }

            System.out.println();
        }
    }

    public static void main(String[] args) {
        print(99, 99);
    }
}
